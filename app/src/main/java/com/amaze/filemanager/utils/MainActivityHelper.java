/*
 * Copyright (C) 2014-2020 Arpit Khurana <arpitkh96@gmail.com>, Vishal Nehra <vishalmeham2@gmail.com>,
 * Emmanuel Messulam<emmanuelbendavid@gmail.com>, Raymond Lai <airwave209gt at gmail.com> and Contributors.
 *
 * This file is part of Amaze File Manager.
 *
 * Amaze File Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.amaze.filemanager.utils;

import java.io.File;
import java.util.ArrayList;

import com.afollestad.materialdialogs.MaterialDialog;
import com.amaze.filemanager.R;
import com.amaze.filemanager.asynchronous.asynctasks.DeleteTask;
import com.amaze.filemanager.filesystem.FileUtil;
import com.amaze.filemanager.filesystem.HybridFileParcelable;

import com.amaze.filemanager.ui.activities.MainActivity;
import com.amaze.filemanager.ui.fragments.MainFragment;
import com.amaze.filemanager.ui.fragments.SearchWorkerFragment;
import com.amaze.filemanager.ui.fragments.TabFragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/** Created by root on 11/22/15, modified by Emmanuel Messulam<emmanuelbendavid@gmail.com> */
public class MainActivityHelper {

  private MainActivity mainActivity;
  private DataUtils dataUtils = DataUtils.getInstance();
  private int accentColor;

  /*
   * A static string which saves the last searched query. Used to retain search task after
   * user presses back button from pressing on any list item of search results
   */
  public static String SEARCH_TEXT;

  public MainActivityHelper(MainActivity mainActivity) {
    this.mainActivity = mainActivity;
    accentColor = mainActivity.getAccent();
  }

  public void showFailedOperationDialog(
      ArrayList<HybridFileParcelable> failedOps, Context context) {
    MaterialDialog.Builder mat = new MaterialDialog.Builder(context);
    mat.title(context.getString(R.string.operation_unsuccesful));
    mat.theme(mainActivity.getAppTheme().getMaterialDialogTheme());
    mat.positiveColor(accentColor);
    mat.positiveText(R.string.cancel);
    String content = context.getString(R.string.operation_fail_following);
    int k = 1;
    for (HybridFileParcelable s : failedOps) {
      content = content + "\n" + (k) + ". " + s.getName(context);
      k++;
    }
    mat.content(content);
    mat.build().show();
  }

  public final BroadcastReceiver mNotificationReceiver =
      new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          if (intent != null) {
            if (intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)) {
              Toast.makeText(mainActivity, "Media Mounted", Toast.LENGTH_SHORT).show();
              String a = intent.getData().getPath();
              if (a != null
                  && a.trim().length() != 0
                  && new File(a).exists()
                  && new File(a).canExecute()) {
                dataUtils.getStorages().add(a);
                mainActivity.getDrawer().refreshDrawer();
              } else {
                mainActivity.getDrawer().refreshDrawer();
              }
            } else if (intent.getAction().equals(Intent.ACTION_MEDIA_UNMOUNTED)) {

              mainActivity.getDrawer().refreshDrawer();
            }
          }
        }
      };


  public String getIntegralNames(String path) {
    String newPath = "";
    switch (Integer.parseInt(path)) {
      case 0:
        newPath = mainActivity.getString(R.string.images);
        break;
      case 1:
        newPath = mainActivity.getString(R.string.videos);
        break;
      case 2:
        newPath = mainActivity.getString(R.string.audio);
        break;
      case 3:
        newPath = mainActivity.getString(R.string.documents);
        break;
      case 4:
        newPath = mainActivity.getString(R.string.apks);
        break;
      case 5:
        newPath = mainActivity.getString(R.string.quick);
        break;
      case 6:
        newPath = mainActivity.getString(R.string.recent);
        break;
    }
    return newPath;
  }

  public void guideDialogForLEXA(String path) {
    final MaterialDialog.Builder x = new MaterialDialog.Builder(mainActivity);
    x.theme(mainActivity.getAppTheme().getMaterialDialogTheme());
    x.title(R.string.needs_access);
    LayoutInflater layoutInflater =
        (LayoutInflater) mainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = layoutInflater.inflate(R.layout.lexadrawer, null);
    x.customView(view, true);
    // textView
    TextView textView = view.findViewById(R.id.description);
    textView.setText(
        mainActivity.getString(R.string.needs_access_summary)
            + path
            + mainActivity.getString(R.string.needs_access_summary1));
    ((ImageView) view.findViewById(R.id.icon)).setImageResource(R.drawable.sd_operate_step);
    x.positiveText(R.string.open)
        .negativeText(R.string.cancel)
        .positiveColor(accentColor)
        .negativeColor(accentColor)
        .onPositive((dialog, which) -> triggerStorageAccessFramework())
        .onNegative(
            (dialog, which) ->
                Toast.makeText(mainActivity, R.string.error, Toast.LENGTH_SHORT).show());
    final MaterialDialog y = x.build();
    y.show();
  }

  private void triggerStorageAccessFramework() {
    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
    mainActivity.startActivityForResult(intent, 3);
  }


  public static final int DOESNT_EXIST = 0;
  public static final int WRITABLE_OR_ON_SDCARD = 1;
  // For Android 5
  public static final int CAN_CREATE_FILES = 2;

  public int checkFolder(final File folder, Context context) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      if (FileUtil.isOnExtSdCard(folder, context)) {
        if (!folder.exists() || !folder.isDirectory()) {
          return DOESNT_EXIST;
        }

        // On Android 5, trigger storage access framework.
        if (!FileUtil.isWritableNormalOrSaf(folder, context)) {
          guideDialogForLEXA(folder.getPath());
          return CAN_CREATE_FILES;
        }

        return WRITABLE_OR_ON_SDCARD;
      } else if (FileUtil.isWritable(new File(folder, "DummyFile"))) {
        return WRITABLE_OR_ON_SDCARD;
      } else return DOESNT_EXIST;
    } else if (Build.VERSION.SDK_INT == 19) {
      if (FileUtil.isOnExtSdCard(folder, context)) {
        // Assume that Kitkat workaround works
        return WRITABLE_OR_ON_SDCARD;
      } else if (FileUtil.isWritable(new File(folder, "DummyFile"))) {
        return WRITABLE_OR_ON_SDCARD;
      } else return DOESNT_EXIST;
    } else if (FileUtil.isWritable(new File(folder, "DummyFile"))) {
      return WRITABLE_OR_ON_SDCARD;
    } else {
      return DOESNT_EXIST;
    }
  }


  public void deleteFiles(ArrayList<HybridFileParcelable> files) {
    if (files == null || files.size() == 0) return;
    if (files.get(0).isSmb()) {
      new DeleteTask(mainActivity).execute((files));
      return;
    }
    int mode = checkFolder(new File(files.get(0).getPath()).getParentFile(), mainActivity);
    if (mode == 2) {
      mainActivity.oparrayList = (files);
      mainActivity.operation = DataUtils.DELETE;
    } else if (mode == 1 || mode == 0) new DeleteTask(mainActivity).execute((files));
    else Toast.makeText(mainActivity, R.string.not_allowed, Toast.LENGTH_SHORT).show();
  }

  /** Retrieve a path with {@link OTGUtil#PREFIX_OTG} as prefix */
  public String parseOTGPath(String path) {
    if (path.contains(OTGUtil.PREFIX_OTG)) return path;
    else return OTGUtil.PREFIX_OTG + path.substring(path.indexOf(":") + 1);
  }


  /**
   * Creates a fragment which will handle the search AsyncTask {@link SearchWorkerFragment}
   *
   * @param query the text query entered the by user
   */
  public void search(SharedPreferences sharedPrefs, String query) {
    TabFragment tabFragment = mainActivity.getTabFragment();
    if (tabFragment == null) return;
    final MainFragment ma = (MainFragment) tabFragment.getCurrentTabFragment();
    final String fpath = ma.getCurrentPath();

    /*SearchTask task = new SearchTask(ma.searchHelper, ma, query);
    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, fpath);*/
    // ma.searchTask = task;
    SEARCH_TEXT = query;
    FragmentManager fm = mainActivity.getSupportFragmentManager();
    SearchWorkerFragment fragment =
        (SearchWorkerFragment) fm.findFragmentByTag(MainActivity.TAG_ASYNC_HELPER);

    if (fragment != null) {
      if (fragment.searchAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
        fragment.searchAsyncTask.cancel(true);
      }
      fm.beginTransaction().remove(fragment).commit();
    }

    addSearchFragment(
        fm,
        new SearchWorkerFragment(),
        fpath,
        query,
        ma.openMode,
        mainActivity.isRootExplorer(),
        sharedPrefs.getBoolean(SearchWorkerFragment.KEY_REGEX, false),
        sharedPrefs.getBoolean(SearchWorkerFragment.KEY_REGEX_MATCHES, false));
  }

  /**
   * Adds a search fragment that can persist it's state on config change
   *
   * @param fragmentManager fragmentManager
   * @param fragment current fragment
   * @param path current path
   * @param input query typed by user
   * @param openMode defines the file type
   * @param rootMode is root enabled
   * @param regex is regular expression search enabled
   * @param matches is matches enabled for patter matching
   */
  public static void addSearchFragment(
      FragmentManager fragmentManager,
      Fragment fragment,
      String path,
      String input,
      OpenMode openMode,
      boolean rootMode,
      boolean regex,
      boolean matches) {
    Bundle args = new Bundle();
    args.putString(SearchWorkerFragment.KEY_INPUT, input);
    args.putString(SearchWorkerFragment.KEY_PATH, path);
    args.putInt(SearchWorkerFragment.KEY_OPEN_MODE, openMode.ordinal());
    args.putBoolean(SearchWorkerFragment.KEY_ROOT_MODE, rootMode);
    args.putBoolean(SearchWorkerFragment.KEY_REGEX, regex);
    args.putBoolean(SearchWorkerFragment.KEY_REGEX_MATCHES, matches);

    fragment.setArguments(args);
    fragmentManager.beginTransaction().add(fragment, MainActivity.TAG_ASYNC_HELPER).commit();
  }
}
